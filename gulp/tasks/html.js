module.exports = (gulp, plugins, browser) => {
    const config = require('../config');

    return () => gulp
        .src(config.html.src)
        .pipe(gulp.dest(config.dir.dist))
        .pipe(browser.stream())
};
