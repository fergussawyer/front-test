module.exports = (gulp, plugins, browser) => {
    const config = require('../config');

    return () => browser.init({
        server: {
            baseDir: config.dir.dist
        }
    });
};
