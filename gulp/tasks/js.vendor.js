module.exports = (gulp, plugins, browser) => {
    const config = require('../config');

    return () => gulp
        .src(config.js.src.vendor)
        .pipe(plugins.plumber())
        .pipe(plugins.concat('vendor.min.js'))
        .on('error', plugins.notify.onError(error => `Error: ${error.message}`))
        .pipe(gulp.dest(config.js.dist))
        .pipe(browser.stream())
};
