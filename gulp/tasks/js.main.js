module.exports = (gulp, plugins, browser) => {
    const config = require('../config');

    return () => gulp
        .src(config.js.src.main)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.plumber())
        .pipe(plugins.jsmin())
        .pipe(plugins.concat('scripts.min.js'))
        .on('error', plugins.notify.onError(error => `Error: ${error.message}`))
        .pipe(plugins.sourcemaps.write('./'))
        .pipe(gulp.dest(config.js.dist))
        .pipe(browser.stream())
};
