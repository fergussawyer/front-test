module.exports = (gulp, plugins, browser) => {
    const config = require('../config');
    const sassGlob = require('gulp-sass-glob');

    return () => gulp
        .src(config.style.src)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.plumber())
        .pipe(sassGlob())
        .pipe(
            plugins.sass({
                sourceMap: true,
                outputStyle: 'expanded',
            }),
        )
        .pipe(
            plugins.autoprefixer(),
        )
        .pipe(plugins.csso())
        .pipe(plugins.concat('styles.min.css'))
        .on('error', plugins.notify.onError(error => `Error: ${error.message}`))
        .pipe(plugins.sourcemaps.write('./'))
        .pipe(gulp.dest(config.style.dist))
        .pipe(browser.stream())
};
