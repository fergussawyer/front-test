module.exports = gulp => {
    return () => {
        global.watch = true;
        gulp.watch(['src/**/*.html'], gulp.series('html'))
            .on('all', (event, filepath) => {
                global.changedTempalteFile = filepath.replace(/\\/g, '/');
            });
        gulp.watch(['src/js/**/**/*.js'], gulp.series(['js.main', 'js.vendor']));
        gulp.watch(['src/scss/**/**/*.scss'], gulp.series('style'));
    }
};
