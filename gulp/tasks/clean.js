module.exports = (gulp, plugins) => {
    const config = require('../config');
    const del = require('del');

    return () => plugins.all(
        plugins.cache.clearAll(),
        del(config.dir.dist),
    );
};
