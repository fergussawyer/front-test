const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const browser = require('browser-sync');

const fs = require('fs');
const taskFolder = './gulp/tasks';

const getTask = task => require(`${taskFolder}/${task}`)(gulp, plugins, browser);

fs.readdirSync(taskFolder).forEach(file => {
    let taskName = file.replace('.js', '');
    gulp.task(taskName, getTask(taskName));
});

gulp.task('default', gulp.series(
    gulp.parallel('clean'),
    gulp.parallel('js.main', 'js.vendor', 'style', 'html'),
    gulp.parallel('watch', 'server'),
));
