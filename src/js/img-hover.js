'use strict';

/**
 *  Механизм плавного перемещения изображения по наведению мышкой в пределах блока
 */

function imgHover() {
    const imgContainer = document.querySelector('.img-hover');
    if (imgContainer) {
        const img = document.querySelector('.img-hover__img');
        const btnTop = document.querySelector('.img-hover__btn--top');
        const btnBottom = document.querySelector('.img-hover__btn--bottom');
        const btnRight = document.querySelector('.img-hover__btn--right');
        const btnLeft = document.querySelector('.img-hover__btn--left');
        let top = 0;
        let left = 0;
        let bottomEnd = -(img.clientHeight - imgContainer.clientHeight);
        let rightEnd = -(img.clientWidth - imgContainer.clientWidth);

        function btnBottomMouseover() {
            while (top >= bottomEnd) {
                top -= 1;
                break;
            }
            img.style.top = top + 1 + 'px';
        }

        function btnTopMouseover() {
            while (top <= 0) {
                top += 1;
                break;
            }
            img.style.top = top - 1 + 'px';
        }

        function btnRightMouseover() {
            while (left >= rightEnd) {
                left -= 1;
                break;
            }
            img.style.left = left + 1 + 'px';
        }

        function btnLeftMouseover() {
            while (left <= 0) {
                left += 1;
                break;
            }
            img.style.left = left - 1 + 'px';
        }

        btnBottom.addEventListener('mouseover', function () {
            let setInterBottom = setInterval(btnBottomMouseover, 10);
            btnBottom.addEventListener('mouseout', function () {
                clearInterval(setInterBottom);
            })
        })

        btnTop.addEventListener('mouseover', function () {
            let setInterTop = setInterval(btnTopMouseover, 10);
            btnTop.addEventListener('mouseout', function () {
                clearInterval(setInterTop);
            })
        })

        btnRight.addEventListener('mouseover', function () {
            let setInterRight = setInterval(btnRightMouseover, 10);
            btnRight.addEventListener('mouseout', function () {
                clearInterval(setInterRight);
            })
        })

        btnLeft.addEventListener('mouseover', function () {
            let setInterLeft = setInterval(btnLeftMouseover, 10);
            btnLeft.addEventListener('mouseout', function () {
                clearInterval(setInterLeft);
            })
        })


    }
}

document.addEventListener('DOMContentLoaded', function () {
    imgHover();
});
