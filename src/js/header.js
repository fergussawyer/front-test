'use strict';

/**
 * Зафиксировать шапку вверху окна
 */

function headerFixed() {
    const header = document.querySelector('.header');
    const main = document.querySelector('.main');
    if (header) {
        function headerFixedClass() {
            if (pageYOffset > 160) {
                header.classList.add('fixed');
                main.classList.add('header-fixed');
            } else {
                header.classList.remove('fixed');
                main.classList.remove('header-fixed');
            }
        }
        headerFixedClass();
        window.addEventListener('scroll', function () {
            headerFixedClass();
        });
        window.addEventListener('resize', function () {
            headerFixedClass();
        });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    headerFixed();
});
