'use strict';

/**
 * Табы превращаються в выпадающий список
 */

function tabsAdaptive() {
    const tabsActive = document.querySelector('.tab-header-wrap');
    if (tabsActive) {
        if (document.body.clientWidth < 768) {
            tabsActive.addEventListener('click', function () {
                this.classList.toggle('open');
            });
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    tabsAdaptive();
});

window.addEventListener('resize', function () {
    tabsAdaptive();
});
